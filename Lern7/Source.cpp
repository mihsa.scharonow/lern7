#include<iostream>
const int N = 10;

class Animal
{

public:
	Animal() {}

	virtual void Voice()
	{
		std::cout << "hello\n";
	}

};

class Dog : public Animal
{
public:

	Dog()
	{}

	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{

public:

	Cat() {}

	void Voice() override
	{
		std::cout << "Meow\n";
	}
};

class Monkey : public Animal
{
public:

	Monkey() {}

	void Voice() override
	{
		std::cout << "OaOaOa\n";
	}
};


int main()
{
	Animal* Zoopark[N];

	
	for (int i = 0; i < N; i++)
	{
		if(i % 3 == 0)
			Zoopark[i] = new Cat;
		if (i % 3 == 1)
			Zoopark[i] = new Dog;
		if (i % 3 == 2)
			Zoopark[i] = new Monkey;
	}

	for (int i = 0; i < N; i++)
	{
		Zoopark[i]->Voice();
	}

}